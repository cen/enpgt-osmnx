import logging
from datetime import datetime
from pathlib import Path

from main import do_osmnx

radii = [100, 200, 500, 1000, 2000]
consolidate_intersections_tolerances = [0, 15]
network_types = ["all", "drive", "walk"]  # Supported: "all", "drive", "walk"

prevent_long_calculations = True  # This will skip radii above 1000 with "all" or "walk" networks, which are very slow

osmnx_endpoint = None  # None for default

task_file = Path("task.txt")
output_dir = Path("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))

if __name__ == "__main__":
    for intersection_tolerance in consolidate_intersections_tolerances:
        for network_type in network_types:
            if prevent_long_calculations:
                if network_type == "all" or network_type == "walk" and intersection_tolerance < 10:
                    radii = sorted({min(radius, 1000) for radius in radii})
                    prevented_radii = [radius for radius in radii if radius not in radii]
                    if prevented_radii:
                        logging.warning(
                            f"Args intersection_tolerance={intersection_tolerance} &"
                            f" network_type={network_type} were determined to take too long for radii "
                            f"{radii!s}. Using radii {radii!s} instead."
                        )

            # TODO: Separate directories??
            for radius in radii:
                do_osmnx(
                    task_file,
                    output_dir,
                    radius,
                    True,
                    network_type,
                    intersection_tolerance,
                    osmnx_endpoint,
                )
