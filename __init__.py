import logging
import os.path
import shutil
from dataclasses import dataclass, field
from datetime import datetime
from pathlib import Path
from typing import List, Optional

from pyproj import Transformer

from BaseClasses import ToolDefinition, ToolDefinitionArgs
from tools.OSMnxStreetNetworkTool.main import main


class OSMnxStreetNetworkToolClass(ToolDefinition):
    tool_name: str = "OSMnxStreetNetworkTool"
    metric_name: str = "OSMnx Street Network Stats"
    supports_multipoly_mode = True

    transproj = Transformer.from_crs(
        "WGS84",
        "EPSG:3035",
        always_xy=True,
    )

    @dataclass
    class OSMnxStreetNetworkToolArgs(ToolDefinitionArgs):
        prevent_long_calculations = True
        radii: List[int] = field(default_factory=lambda: [100, 200, 500, 1000, 2000])
        consolidate_intersections_tolerances: List[int] = field(default_factory=lambda: [0, 15])
        network_types: List[str] = field(default_factory=lambda: ["all", "drive", "walk"])
        osmnx_endpoint: Optional[str] = None

    @classmethod
    def supports_coordinate(cls, lat: float, lng: float) -> bool:
        return True

    @classmethod
    def required_setup(cls, _: OSMnxStreetNetworkToolArgs):
        return

    @classmethod
    def main(cls, args: OSMnxStreetNetworkToolArgs) -> Path:
        correct_dir = os.getcwd()

        tool_dir = os.path.dirname(os.path.realpath(__file__))

        output_dirs = []

        for intersection_tolerance in args.consolidate_intersections_tolerances:
            for network_type in args.network_types:
                radii = args.radii

                if args.prevent_long_calculations:
                    if network_type == "all" or network_type == "walk" and intersection_tolerance < 10:
                        radii = sorted({min(radius, 1000) for radius in args.radii})
                        prevented_radii = [radius for radius in args.radii if radius not in radii]
                        if prevented_radii:
                            logging.warning(
                                f"Args intersection_tolerance={intersection_tolerance} &"
                                f" network_type={network_type} were determined to take too long for radii "
                                f"{args.radii!s}. Using radii {radii!s} instead."
                            )

                sysargs = [
                    str(radii).replace(" ", ""),
                    os.path.realpath(args.task_file_location),
                    network_type,
                    f"consolidate_intersections={intersection_tolerance}",
                ]

                if args.osmnx_endpoint is not None:
                    sysargs.append("osm_url=" + args.osmnx_endpoint)

                output_directories_before = []
                if os.path.isdir(os.path.join(tool_dir, "outputs")):
                    output_directories_before = [
                        os.path.join(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
                    ]

                os.chdir(tool_dir)

                main(sysargs)

                os.chdir(correct_dir)

                output_directories_after = []
                if os.path.isdir(os.path.join(tool_dir, "outputs")):
                    output_directories_after = [
                        os.path.join(tool_dir, "outputs", p) for p in os.listdir(os.path.join(tool_dir, "outputs"))
                    ]

                extras = f"_networktype_{network_type}_consolidatedIntersections_{intersection_tolerance}"

                new_directory = list(set(output_directories_after) - set(output_directories_before))
                assert len(new_directory) == 1, "Somehow, multiple output dirs were found."
                new_directory = new_directory[0]
                renamed = os.path.normpath(new_directory) + extras
                os.rename(new_directory, renamed)

                output_dirs.append(renamed)

        full_output_dir = Path(datetime.now().strftime("%d-%m-%Y_%H-%M-%S")).resolve()
        for output_dir in output_dirs:
            shutil.move(output_dir, full_output_dir)

        return full_output_dir
