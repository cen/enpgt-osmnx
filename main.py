import ast
import logging
import os
import re
import statistics
import sys
from datetime import datetime
from multiprocessing import Pool
from pathlib import Path
from typing import List, Optional

import networkx as nx
import osmnx as ox
import osmnx.bearing
import osmnx.plot
import osmnx.settings
import osmnx.stats
import osmnx.truncate
import pyproj
import shapely.ops
from shapely import geometry
from tqdm import tqdm

BUFFER_RADIUS = 500


def get_network(
    desired_radius: int, lat: float, lng: float, network_type="all", intersection_tolerance=0, do_betweenness=False
):
    buffer_poly = geometry.Point(0, 0).buffer(desired_radius, resolution=2)
    crs_aeqd = pyproj.Proj(proj="aeqd", datum="WGS84", lon_0=lng, lat_0=lat, units="m")

    network = ox.graph_from_point((lat, lng), desired_radius + BUFFER_RADIUS, network_type=network_type)
    network = ox.project_graph(network)

    if intersection_tolerance:
        network = ox.consolidate_intersections(
            network, rebuild_graph=True, tolerance=intersection_tolerance, dead_ends=True, reconnect_edges=True
        )
        spn = ox.stats.count_streets_per_node(network)
        nx.set_node_attributes(network, values=spn, name="street_count")

    graph_crs = pyproj.Proj(network.graph["crs"])
    transproj = pyproj.transformer.Transformer.from_proj(crs_aeqd, graph_crs, always_xy=True)

    buffer_poly_in_local = shapely.ops.transform(transproj.transform, buffer_poly)

    if do_betweenness:
        bc = nx.betweenness_centrality(ox.convert.to_digraph(network), weight="length")
        nx.set_node_attributes(network, bc, "centrality_with_periphery")

    network_truncated = ox.truncate.truncate_graph_polygon(network, buffer_poly_in_local)

    if do_betweenness:
        bc = nx.betweenness_centrality(ox.convert.to_digraph(network_truncated), weight="length")
        nx.set_node_attributes(network_truncated, bc, "centrality_without_periphery")

    return network, network_truncated


def plot_osmnx(network):
    nc = ox.plot.get_node_colors_by_attr(network, "street_count", cmap="plasma")

    ox.plot_graph(
        network,
        node_color=list(nc),
        node_size=30,
        node_zorder=2,
        edge_linewidth=0.2,
        edge_color="w",
    )

    nc = ox.plot.get_node_colors_by_attr(network, "centrality_with_periphery", cmap="plasma")
    ox.plot_graph(
        network,
        node_color=list(nc),
        node_size=30,
        node_zorder=2,
        edge_linewidth=0.2,
        edge_color="w",
    )

    nc = ox.plot.get_node_colors_by_attr(network, "centrality_without_periphery", cmap="plasma")
    ox.plot_graph(
        network,
        node_color=list(nc),
        node_size=30,
        node_zorder=2,
        edge_linewidth=0.2,
        edge_color="w",
    )


def do_single_osmnx(
    geo_id: str,
    radius: int,
    lat: float,
    lng: float,
    network_type="all",
    plot=False,
    intersections=0,
    do_betweenness=False,
):
    try:
        network_with_periphery, network = get_network(
            radius,
            lat,
            lng,
            network_type=network_type,
            intersection_tolerance=intersections,
            do_betweenness=do_betweenness,
        )

        if plot:
            plot_osmnx(network)

        basic_stats = osmnx.basic_stats(network)
    except nx.exception.NetworkXPointlessConcept:
        logging.error(f"There are not enough nodes within a {radius}m radius around {lat},{lng}, either 0 or 1.")
        return ".", "."
    except ValueError as e:
        logging.error(f"The following error happened while trying to get the network graph for {lat},{lng}: {e}")
        return ".", "."

    if do_betweenness:
        centrality = nx.get_node_attributes(network, "centrality_with_periphery")
        centrality_without_periphery = nx.get_node_attributes(network, "centrality_without_periphery")

        avg1, avg2 = statistics.mean(centrality.values()), statistics.mean(centrality_without_periphery.values())
        max1, max2 = max(centrality.values()), max(centrality_without_periphery.values())
        min1, min2 = min(centrality.values()), min(centrality_without_periphery.values())
        stddev1, stddev2 = (
            statistics.stdev(centrality.values()),
            statistics.stdev(centrality_without_periphery.values()),
        )
        median1, median2 = (
            statistics.median(centrality.values()),
            statistics.median(centrality_without_periphery.values()),
        )

        basic_stats[f"all_centralities_considering_{BUFFER_RADIUS}m_periphery"] = (
            "{ " + " | ".join(str(token) for token in sorted(centrality.values())) + " }"
        )
        basic_stats[f"avg_centrality_considering_{BUFFER_RADIUS}m_periphery"] = avg1
        basic_stats[f"max_centrality_considering_{BUFFER_RADIUS}m_periphery"] = max1
        basic_stats[f"min_centrality_considering_{BUFFER_RADIUS}m_periphery"] = min1
        basic_stats[f"stddev_centrality_considering_{BUFFER_RADIUS}m_periphery"] = stddev1
        basic_stats[f"median_centrality_considering_{BUFFER_RADIUS}m_periphery"] = median1

        basic_stats["all_centralities"] = (
            "{ " + " | ".join(str(token) for token in sorted(centrality_without_periphery.values())) + " }"
        )
        basic_stats["avg_centrality"] = avg2
        basic_stats["max_centrality"] = max2
        basic_stats["min_centrality"] = min2
        basic_stats["stddev_centrality"] = stddev2
        basic_stats["median_centrality"] = median2

    unprojected_network = ox.project_graph(network, to_crs="WGS84")
    ox.add_edge_bearings(unprojected_network)
    basic_stats["bearing_entropy"] = ox.bearing.orientation_entropy(unprojected_network.to_undirected())

    return geo_id, basic_stats


def do_single_osmnx_star(star):
    return do_single_osmnx(*star)


def initializer(osm_url: Optional[str]):
    if osm_url is None:
        logging.info("Default OSM endpoint used.")
        return

    osmnx.settings.overpass_endpoint = osm_url
    osmnx.settings.overpass_rate_limit = False
    osmnx.settings.use_cache = False
    logging.info(f"Using OSM endpoint {osm_url}")


def do_osmnx(
    infile: Path,
    output_dir: Path,
    radius: int,
    do_betweenness: bool,
    network_type="all",
    consolidate_intersections=0,
    osm_url: Optional[str] = None,
):
    print("---")
    print(f"Getting osmnx stats for file {infile} with radius {radius}m.")
    if consolidate_intersections:
        print(f"consolidate_intersections mode is on with value {consolidate_intersections}.")

    output_dir.mkdir(parents=True, exist_ok=True)

    id_lat_lngs = []

    with open(infile) as task_file:
        for line in task_file.readlines():
            line = line.strip()
            line_split = re.split("[;,\\t]", line)

            try:
                id_lat_lngs.append((line_split[0], float(line_split[1]), float(line_split[2])))
            except Exception:
                print(f"Could not interpet line: {line}")

    starmap = [
        (geo_id, radius, lat, lng, network_type, False, consolidate_intersections, do_betweenness)
        for geo_id, lat, lng in id_lat_lngs
    ]

    with Pool(initializer=initializer, initargs=(osm_url,)) as pool:
        output = list(tqdm(pool.imap(do_single_osmnx_star, starmap), total=len(starmap)))

    output_dict = dict(output)
    if "." in output_dict:
        del output_dict["."]

    extras = f"_networktype_{network_type}"
    if consolidate_intersections:
        extras += f"_consolidatedIntersections_{consolidate_intersections}"

    output_file_path = os.path.join(output_dir, f"output_{radius}{extras}.csv")

    with open(output_file_path, "w") as output_file:
        with open(infile) as task_file:
            keys = ""
            if output_dict:
                keys = ",".join(next(iter(output_dict.values())).keys())

            keys = keys.replace(",n,", ",nodes,").replace(",m,", ",edges,")

            output_file.write(f"id,{keys}\n")

            for line in task_file.readlines():
                line = line.strip()
                line_split = re.split("[;,\\t]", line)

                if line_split[0] not in output_dict:
                    continue

                values = ",".join(str(value).replace(", ", " | ") for key, value in output_dict[line_split[0]].items())

                output_file.write(f"{line_split[0]},{values}\n")


def output_readme(file_path: str, radii: List[int]):
    logging.warning("Output Readme not implemented")
    return


def main(args):
    default_task_file = Path("task.txt")

    radii = [100, 200, 500, 1000, 2000]

    consolidate_intersections = 0

    network_type = "all"

    osm_url = None

    do_betweenness = False

    for arg in args:
        if arg.startswith("[") and arg.endswith("]"):
            radii = ast.literal_eval(arg)

        elif arg.startswith("osm_url="):
            osm_url = arg[8:]
            if not osm_url.endswith("api"):
                osm_url = osm_url.rstrip("/") + "/api"

        elif arg.startswith("do_betweenness="):
            do_betweenness = arg[15:].lower() in {"yes", "true", "t", "y"}

        elif arg.isnumeric():
            radii = [int(arg)]

        elif arg in {"drive", "walk", "bike", "all"}:
            network_type = arg

        elif arg.startswith("consolidate_intersections"):
            if arg == "consolidate_intersections":
                consolidate_intersections = 15
                logging.info(
                    f"consolidate_intersections was set without a value. Defaulting to {consolidate_intersections}."
                )
            else:
                arg_split = arg.split("=")
                if len(arg_split) != 2:
                    raise ValueError(f'Don\'t understand arg "{arg}"')
                try:
                    consolidate_intersections = float(arg_split[1])
                except ValueError:
                    raise ValueError(f'Don\'t understand value "{arg_split}" for consolidate_intersections')

        else:
            default_task_file = Path(arg)

    if not default_task_file.is_file():
        print(f"No task file was specified, and {default_task_file} was not found. Don't know what to work on.")
        return

    output_dir = Path("outputs", datetime.now().strftime("%d-%m-%Y_%H-%M-%S"))

    for radius in radii:
        do_osmnx(
            default_task_file,
            output_dir,
            radius,
            do_betweenness,
            network_type,
            consolidate_intersections,
            osm_url,
        )

    output_readme(os.path.join(output_dir, "readme.txt"), radii)


if __name__ == "__main__":
    main(sys.argv[1:])

# TODO: Try out truncate_graph_dist. Need to find node closest to center, probably by pyproj.transformer-ing the center
